//
//  ViewController.h
//  designs
//
//  Created by Ankur on 01/09/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *fontLabel;
- (IBAction)radiusStepper:(UIStepper *)sender;
- (IBAction)opacityStepper:(UIStepper *)sender;
- (IBAction)widthStepper:(UIStepper *)sender;
- (IBAction)heightStepper:(UIStepper *)sender;
- (IBAction)fontStepper:(UIStepper *)sender;
- (IBAction)redStepper:(UIStepper *)sender;
- (IBAction)greenStepper:(UIStepper *)sender;
- (IBAction)BlueStepper:(UIStepper *)sender;
- (IBAction)saveBtnClicked:(id)sender;
- (IBAction)fontTfEndEditing:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *screenShotView;
@property (weak, nonatomic) IBOutlet UIImageView *rainbowImageVU;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *RedLabel;
@property (weak, nonatomic) IBOutlet UILabel *GreenLabel;
@property (weak, nonatomic) IBOutlet UILabel *BlueLabel;
@property (weak, nonatomic) IBOutlet UILabel *opacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;
@property (weak, nonatomic) IBOutlet UILabel *OffwidthLabel;
@property (weak, nonatomic) IBOutlet UILabel *OffHeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *fontSizeLabel;
@property (weak, nonatomic) IBOutlet UIStepper *rstepper;
@property (weak, nonatomic) IBOutlet UIStepper *gstepper;
@property (weak, nonatomic) IBOutlet UIStepper *bstepper;


@end

