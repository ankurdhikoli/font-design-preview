//
//  ViewController.m
//  designs
//
//  Created by Ankur on 01/09/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "ViewController.h"
#import "GradientView.h"

#define kColor(R,G,B,A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:alpha/255.0]


@interface ViewController (){
    double width;
    double height;
    int red;
    int green;
    int blue;
    NSArray *fontArray;
}

@property (weak, nonatomic) IBOutlet GradientView *gradientView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    fontArray= [UIFont familyNames];
    fontArray = [fontArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
  //  _fontLabel.layer.shadowOpacity = 1.0;
   // _fontLabel.layer.shadowRadius = 0.0;
    _fontLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    //_fontLabel.layer.shadowOffset = CGSizeMake(0.0, -5.0);
    // Do any additional setup after loading the view, typically from a nib.
    
    UIPanGestureRecognizer * tapRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
   }

- (UIColor *)colorForTouchPoint:(CGPoint)touchPoint view:(UIView *)view
{
    UIColor *color = nil;
    UIGraphicsBeginImageContext(view.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    CGPoint lTouchPoint = [view convertPoint:touchPoint fromView:self.view];
    return [self colorFromImage:image atPoint:lTouchPoint];
}

- (UIColor *)colorFromImage:(UIImage *)image atPoint:(CGPoint)point
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    CGImageRef imageRef = [image CGImage];
    NSUInteger w = CGImageGetWidth(imageRef);
    NSUInteger h = CGImageGetHeight(imageRef);

    unsigned char *rawData = malloc(h * w * 4);
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * w;
    NSUInteger bitsPerComponent = 8;
    
    CGContextRef context = CGBitmapContextCreate(rawData,
                                                 w, h,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), imageRef);
    CGContextRelease(context);
    
    //GET PIXEL FROM POINT
    int index = 4*((w*round(point.y))+round(point.x));
    
    int R = rawData[index];
    int G = rawData[index+1];
    int B = rawData[index+2];
    int alpha = rawData[index+3];
    
    NSLog(@"%d   %d   %d %d", R, G, B, alpha);
    
    free(rawData);

    red = R;
    green = G;
    blue = B;

    
    return kColor(R, G, B, alpha);
    
}

// http://stackoverflow.com/questions/4616778/ios-detect-the-color-of-a-pixel
//{
//    int byteIndex = 0;
//    
//    for(int ii = 0 ; ii < width * height ; ++ii)
//    {
//        rawData[byteIndex] = (char)(newPixelValue);
//        rawData[byteIndex+1] = (char)(newPixelValue);
//        rawData[byteIndex+2] = (char)(newPixelValue);
//        
//        byteIndex += 4;
//    }
//    
//    
//    ctx = CGBitmapContextCreate(rawData,
//                                CGImageGetWidth( imageRef ),
//                                CGImageGetHeight( imageRef ),
//                                8,
//                                CGImageGetBytesPerRow( imageRef ),
//                                CGImageGetColorSpace( imageRef ),
//                                kCGImageAlphaPremultipliedLast );
//    
//    imageRef = CGBitmapContextCreateImage(ctx);
//    UIImage* rawImage = [UIImage imageWithCGImage:imageRef];
//    
//    CGContextRelease(ctx);  
//    
//    image = rawImage;
//}

- (void)tapGesture:(UIPanGestureRecognizer *)recognizer
{    
    CGPoint point = [recognizer locationInView:self.view];
    
    UIView *targetView = nil;
    UIColor *color = nil;
    if (CGRectContainsPoint(self.rainbowImageVU.frame, point))
    {
        targetView = self.rainbowImageVU;
        color = [self colorForTouchPoint:point view:targetView];

        self.gradientView.startColor = color;
        self.gradientView.endColor = [UIColor blackColor];
        [self.gradientView setNeedsDisplay];
        
    }
    else if (CGRectContainsPoint(self.gradientView.frame, point))
    {
        targetView = self.gradientView;
        color = [self colorForTouchPoint:point view:targetView];

    }
    
    
    if (color)
    {
        _screenShotView.backgroundColor = color;
        _rstepper.value = red;
        _gstepper.value = green;
        _bstepper.value = blue;
    }
    
    
    _RedLabel.text=[NSString stringWithFormat:@"R = %i",red];
    _GreenLabel.text=[NSString stringWithFormat:@"G = %i",green];
    _BlueLabel.text=[NSString stringWithFormat:@"B = %i",blue];

    
   
    
    
//    if (alpha == 0)
//    {
//        // Here is tap out of text
//    }
//    else
//    {
//        // Here is tap right into text
//    }

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)radiusStepper:(UIStepper *)sender {
    
    _fontLabel.layer.shadowRadius = sender.value;
    _radiusLabel.text=[NSString stringWithFormat:@"R = %i",(int)sender.value];

}

- (IBAction)opacityStepper:(UIStepper *)sender {
    
    _fontLabel.layer.shadowOpacity = sender.value;
    _opacityLabel.text=[NSString stringWithFormat:@"O = %i",(int)sender.value];

}

- (IBAction)widthStepper:(UIStepper *)sender {
    
    width = sender.value;
     _fontLabel.layer.shadowOffset = CGSizeMake(width, height);
    _OffwidthLabel.text=[NSString stringWithFormat:@"OW = %i",(int)sender.value];

}

- (IBAction)heightStepper:(UIStepper *)sender {
     height =  sender.value;
    _fontLabel.layer.shadowOffset = CGSizeMake(width, height);
    _OffHeightLabel.text=[NSString stringWithFormat:@"OH = %i",(int)sender.value];

}

- (IBAction)fontStepper:(UIStepper *)sender {
    
    [_fontLabel setFont:[UIFont fontWithName:_fontLabel.font.fontName size:[sender value]]];
    _fontSizeLabel.text=[NSString stringWithFormat:@"F = %i",(int)sender.value];

}

- (IBAction)redStepper:(UIStepper *)sender {
    red=(int)sender.value;

    _RedLabel.text=[NSString stringWithFormat:@"R = %i",(int)sender.value];
_screenShotView.backgroundColor=[UIColor colorWithRed:red/256.0 green:green/256.0 blue:blue/256.0 alpha:1.0];
}

- (IBAction)greenStepper:(UIStepper *)sender {
    green=(int)sender.value;

    _GreenLabel.text=[NSString stringWithFormat:@"G = %i",(int)sender.value];
_screenShotView.backgroundColor=[UIColor colorWithRed:red/256.0 green:green/256.0 blue:blue/256.0 alpha:1.0];}

- (IBAction)BlueStepper:(UIStepper *)sender {
    
    blue=(int)sender.value;
    
    
    _BlueLabel.text=[NSString stringWithFormat:@"B = %i",(int)sender.value];
_screenShotView.backgroundColor=[UIColor colorWithRed:red/256.0 green:green/256.0 blue:blue/256.0 alpha:1.0];
}

- (IBAction)saveBtnClicked:(id)sender {
    
    UIGraphicsBeginImageContextWithOptions(_screenShotView.bounds.size, self.view.opaque, 0.0);
    [_screenShotView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageWriteToSavedPhotosAlbum(image,
                                   nil,
                                   nil,
                                   nil);
    
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)fontTfEndEditing:(UITextField *)sender {
    _fontLabel.text=sender.text;
    [_tableView reloadData];
}



//
//– (NSInteger)tableView: (UITableView *)tableView numberOfRowsInSection: (NSInteger)section
//{
//    return 10;
//}
//- See more at: http://www.colejoplin.com/2012/09/28/ios-tutorial-basics-of-table-views-and-prototype-cells-in-storyboards/#sthash.TzqWIRle.dpuf

//– (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath {
//}
//- See more at: http://www.colejoplin.com/2012/09/28/ios-tutorial-basics-of-table-views-and-prototype-cells-in-storyboards/#sthash.TzqWIRle.dpuf




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return fontArray.count;    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FontCell"];
    [cell.textLabel setFont:[UIFont fontWithName:[fontArray objectAtIndex:indexPath.row] size:cell.textLabel.font.pointSize]];
    cell.textLabel.text=_fontLabel.text;
    cell.detailTextLabel.text=[fontArray objectAtIndex:indexPath.row];
   // UILabel *lblName = (UILabel *)[cell viewWithTag:100];
   // [lblName setText:@"hello"];
    return cell;

}           // returns nil if cell is not visible or index path is out of range


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [_fontLabel setFont:[UIFont fontWithName:[fontArray objectAtIndex:indexPath.row] size:_fontLabel.font.pointSize]];

    
//    Yourstring=[catagorry objectAtIndex:indexPath.row];
//    
//    //Pushing next view
//    cntrSecondViewController *cntrinnerService = [[cntrSecondViewController alloc] initWithNibName:@"cntrSecondViewController" bundle:nil];
//    [self.navigationController pushViewController:cntrinnerService animated:YES];
    
}

@end
