//
//  AppDelegate.h
//  designs
//
//  Created by Ankur on 01/09/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

